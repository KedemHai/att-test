import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { InfectionDetailsComponent } from './infection-details/infection-details.component'

const routes: Routes = [
  {
    path: 'infections/:id',
    component: InfectionDetailsComponent
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
