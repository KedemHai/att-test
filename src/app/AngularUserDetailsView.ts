import { UserDetailsUserActions, UserDetailsView } from '../Att/UI/UserDetails/UserDetailsContract'
import { AppComponent } from './app.component'

class AngularUserDetailsView implements UserDetailsView {
  private readonly component: AppComponent

  public constructor (component: AppComponent) {
    this.component = component
  }

  public attach (userActions: UserDetailsUserActions): void {
    this.component.initiated.subscribe(() => {
      userActions.loaded()
    })
  }

  public setNumberOfInfections (n: number): void {
    this.component.countInfections = String(n)
  }

  public setUserName (name: string): void {
    this.component.userName = name
  }
}

export default AngularUserDetailsView