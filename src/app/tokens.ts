import { InjectionToken } from '@angular/core'
import { UserGateway } from '../Att/App/UserGateway/UserGateway'
import { InfectionGateway } from '../Att/App/InfectionGateway/InfectionGateway'
import { GetUserDetailsInput } from '../Att/App/GetUserDetails/GetUserDetailsContract'
import MockApi from '../Att/Infrastructure/MockApi/MockApi'
import { SummarizeUserInfectionsInput } from '../Att/App/SummarizeUserInfections/SummarizeUserInfectionsContract'
import { GetInfectionDetailsInput } from '../Att/App/GetInfectionDetails/GetInfectionDetailsContract'

export const GET_USER_DETAILS = new InjectionToken<GetUserDetailsInput>('')
export const SUMMARIZE_USER_INFECTIONS = new InjectionToken<SummarizeUserInfectionsInput>('')
export const GET_INFECTION_DETAILS = new InjectionToken<GetInfectionDetailsInput>('')
export const USER_GATEWAY = new InjectionToken<UserGateway>('')
export const INFECTION_GATEWAY = new InjectionToken<InfectionGateway>('')
export const MOCK_API = new InjectionToken<MockApi>('')