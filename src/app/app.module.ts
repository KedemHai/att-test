import { BrowserModule } from '@angular/platform-browser'
import { Injector, NgModule } from '@angular/core'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import {
  GET_INFECTION_DETAILS,
  GET_USER_DETAILS,
  INFECTION_GATEWAY,
  MOCK_API,
  SUMMARIZE_USER_INFECTIONS,
  USER_GATEWAY
} from './tokens'
import GetUserDetails from '../Att/App/GetUserDetails/GetUserDetails'
import MockApiUserGateway from '../Att/Infrastructure/MockApi/MockApiUserGateway'
import MockApi from '../Att/Infrastructure/MockApi/MockApi'
import MockApiInfectionGateway from '../Att/Infrastructure/MockApi/MockApiInfectionGateway'
import { InfectionListComponent } from './infection-list/infection-list.component'
import { NoopAnimationsModule } from '@angular/platform-browser/animations'
import { OverlayModule } from '@angular/cdk/overlay'
import { DatetimeTextComponent } from './text/datetime-text/datetime-text.component'
import { TextComponent } from './text/text.component'
import SummarizeUserInfections from '../Att/App/SummarizeUserInfections/SummarizeUserInfections'
import { InfectionDetailsComponent } from './infection-details/infection-details.component'
import GetInfectionDetails from '../Att/App/GetInfectionDetails/GetInfectionDetails'

@NgModule({
  declarations: [
    AppComponent,
    InfectionListComponent,
    DatetimeTextComponent,
    TextComponent,
    InfectionDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    OverlayModule
  ],
  providers: [
    {
      provide: MOCK_API,
      useFactory: () => {
        return new MockApi()
      }
    },
    {
      provide: USER_GATEWAY,
      useFactory: (injector: Injector) => {
        return new MockApiUserGateway(injector.get(MOCK_API))
      },
      deps: [
        Injector
      ]
    },
    {
      provide: INFECTION_GATEWAY,
      useFactory: (injector: Injector) => {
        return new MockApiInfectionGateway(injector.get(MOCK_API))
      },
      deps: [
        Injector
      ]
    },
    {
      provide: GET_USER_DETAILS,
      useFactory: (injector: Injector) => {
        const userGateway = injector.get(USER_GATEWAY)
        const infectionGateway = injector.get(INFECTION_GATEWAY)
        return new GetUserDetails(userGateway, infectionGateway)
      },
      deps: [
        Injector
      ]
    },
    {
      provide: SUMMARIZE_USER_INFECTIONS,
      useFactory: (injector: Injector) => {
        const infectionGateway = injector.get(INFECTION_GATEWAY)
        return new SummarizeUserInfections(infectionGateway)
      },
      deps: [
        Injector
      ]
    },
    {
      provide: GET_INFECTION_DETAILS,
      useFactory: (injector: Injector) => {
        const infectionGateway = injector.get(INFECTION_GATEWAY)
        return new GetInfectionDetails(infectionGateway)
      },
      deps: [
        Injector
      ]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
