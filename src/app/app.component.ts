import { Component, Injector, OnInit } from '@angular/core'
import { UserDetailsUserActions, UserDetailsView } from '../Att/UI/UserDetails/UserDetailsContract'
import AngularUserDetailsView from './AngularUserDetailsView'
import { ReplaySubject, Subject } from 'rxjs'
import UserDetailsPresenter from '../Att/UI/UserDetails/UserDetailsPresenter'
import { GetUserDetailsInput } from '../Att/App/GetUserDetails/GetUserDetailsContract'
import { GET_USER_DETAILS } from './tokens'

@Component({
  selector: 'att-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public readonly userId: string = '1'
  public userName: string | null = null
  public countInfections: string | null = null

  public initiated: Subject<void> = new ReplaySubject<void>(1)

  private readonly injector: Injector

  public constructor (injector: Injector) {
    this.injector = injector
  }

  public ngOnInit (): void {
    const view: UserDetailsView = new AngularUserDetailsView(this)
    const getUserDetails: GetUserDetailsInput = this.injector.get(GET_USER_DETAILS)
    const presenter: UserDetailsUserActions = new UserDetailsPresenter(view, this.userId, getUserDetails)
    view.attach(presenter)

    this.initiated.next()
  }
}
