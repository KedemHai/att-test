import { Component, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core'
import { TextUserActions, TextView } from '../../../Att/UI/Text/TextContract'
import { TextComponent } from '../text.component'
import AngularTextView from '../AngularTextView'
import DateTimePresenter from '../../../Att/UI/DateTime/DateTimePresenter'

@Component({
  selector: 'att-datetime-text',
  templateUrl: './datetime-text.component.html',
  styleUrls: ['./datetime-text.component.scss']
})
export class DatetimeTextComponent implements OnChanges {
  @Input() public datetime?: Date

  @ViewChild('textComponent', {read: TextComponent, static: true})
  private readonly textComponent?: TextComponent

  public ngOnChanges (changes: SimpleChanges): void {
    if (changes.hasOwnProperty('datetime')) {
      const value = changes.datetime.currentValue as DatetimeTextComponent['datetime']
      if (typeof value !== 'undefined') {
        const view: TextView = new AngularTextView(this.textComponent as TextComponent)
        const presenter: TextUserActions = new DateTimePresenter(view, value)
        view.attach(presenter)
      }
    }
  }
}
