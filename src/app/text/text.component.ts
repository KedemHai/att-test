import { Component, OnInit } from '@angular/core'
import { ReplaySubject, Subject } from 'rxjs'

@Component({
  selector: 'att-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent implements OnInit {
  public text?: string
  public initiated: Subject<void> = new ReplaySubject<void>()

  public ngOnInit (): void {
    this.initiated.next()
  }
}
