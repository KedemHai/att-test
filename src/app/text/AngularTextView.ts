import { TextUserActions, TextView } from '../../Att/UI/Text/TextContract'
import { TextComponent } from './text.component'

class AngularTextView implements TextView {
  private readonly component: TextComponent

  public constructor (component: TextComponent) {
    this.component = component
  }

  public attach (userActions: TextUserActions): void {
    this.component.initiated.subscribe(() => {
      userActions.loaded()
    })
  }

  public setText (text: string): void {
    this.component.text = text
  }
}

export default AngularTextView