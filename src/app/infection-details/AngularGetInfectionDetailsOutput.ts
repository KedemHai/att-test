import { GetInfectionDetailsOutput } from '../../Att/App/GetInfectionDetails/GetInfectionDetailsContract'
import { Infection } from '../../Att/Domain/Infection'
import { InfectionDetailsComponent } from './infection-details.component'

class AngularGetInfectionDetailsOutput implements GetInfectionDetailsOutput {
  private readonly component: InfectionDetailsComponent

  public constructor (component: InfectionDetailsComponent) {
    this.component = component
  }

  public presentInfectionDetails (response: Infection): void {
    this.component.details = response
  }
}

export default AngularGetInfectionDetailsOutput