import { Component, Injector, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import {
  GetInfectionDetailsInput,
  GetInfectionDetailsOutput
} from '../../Att/App/GetInfectionDetails/GetInfectionDetailsContract'
import { GET_INFECTION_DETAILS } from '../tokens'
import { Infection } from '../../Att/Domain/Infection'
import AngularGetInfectionDetailsOutput from './AngularGetInfectionDetailsOutput'

@Component({
  selector: 'att-infection-details',
  templateUrl: './infection-details.component.html',
  styleUrls: ['./infection-details.component.scss']
})
export class InfectionDetailsComponent implements OnInit {
  private readonly route: ActivatedRoute
  private readonly injector: Injector
  public details: Infection | null = null

  public constructor (route: ActivatedRoute, injector: Injector) {
    this.route = route
    this.injector = injector
  }

  public ngOnInit (): void {
    this.route.paramMap.subscribe(value => {
      const infectionId: string | null = value.get('id')
      if (infectionId === null) {
        return
      }
      const getInfectionDetails: GetInfectionDetailsInput = this.injector.get(GET_INFECTION_DETAILS)
      const output: GetInfectionDetailsOutput = new AngularGetInfectionDetailsOutput(this)
      getInfectionDetails.execute(infectionId, output)
    })
  }
}
