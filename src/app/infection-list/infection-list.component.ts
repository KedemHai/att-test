import { Component, Injector, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core'
import {
  SummarizeUserInfectionsInput,
  UserInfectionSummaryResponse
} from '../../Att/App/SummarizeUserInfections/SummarizeUserInfectionsContract'
import { SUMMARIZE_USER_INFECTIONS } from '../tokens'
import { InfectionListUserActions, InfectionListView } from '../../Att/UI/InfectionList/InfectionListContract'
import AngularInfectionListView from './AngularInfectionListView'
import { ReplaySubject, Subject } from 'rxjs'
import InfectionListPresenter from '../../Att/UI/InfectionList/InfectionListPresenter'

@Component({
  selector: 'att-infection-list',
  templateUrl: './infection-list.component.html',
  styleUrls: ['./infection-list.component.scss']
})
export class InfectionListComponent implements OnChanges, OnInit {
  @Input() public userId?: string
  public list: UserInfectionSummaryResponse[] = []

  public initiated: Subject<void> = new ReplaySubject<void>(1)

  private readonly injector: Injector

  public constructor (injector: Injector) {
    this.injector = injector
  }

  public ngOnChanges (changes: SimpleChanges): void {
    if (changes.hasOwnProperty('userId')) {
      const value = changes.userId.currentValue as InfectionListComponent['userId']
      if (typeof value !== 'undefined') {
        const view: InfectionListView = new AngularInfectionListView(this)
        const summarizeUserInfections: SummarizeUserInfectionsInput = this.injector.get<SummarizeUserInfectionsInput>(SUMMARIZE_USER_INFECTIONS)
        const presenter: InfectionListUserActions = new InfectionListPresenter(view, value, summarizeUserInfections)
        view.attach(presenter)
      }
    }
  }

  public ngOnInit (): void {
    this.initiated.next()
  }
}
