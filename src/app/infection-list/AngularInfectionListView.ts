import { InfectionListUserActions, InfectionListView } from '../../Att/UI/InfectionList/InfectionListContract'
import { InfectionListComponent } from './infection-list.component'
import { UserInfectionSummaryResponse } from '../../Att/App/SummarizeUserInfections/SummarizeUserInfectionsContract'

class AngularInfectionListView implements InfectionListView {
  private readonly component: InfectionListComponent

  public constructor (component: InfectionListComponent) {
    this.component = component
  }

  public attach (userActions: InfectionListUserActions): void {
    this.component.initiated.subscribe(() => {
      userActions.loaded()
    })
  }

  public setList (list: UserInfectionSummaryResponse[]): void {
    this.component.list = list
  }
}

export default AngularInfectionListView