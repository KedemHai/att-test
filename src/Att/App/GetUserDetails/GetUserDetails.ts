import { GetUserDetailsInput, GetUserDetailsOutput, UserDetailsResponse } from './GetUserDetailsContract'
import { UserGateway } from '../UserGateway/UserGateway'
import { InfectionGateway } from '../InfectionGateway/InfectionGateway'
import { User } from '../../Domain/User'

class GetUserDetails implements GetUserDetailsInput {
  private readonly userGateway: UserGateway
  private readonly infectionGateway: InfectionGateway

  public constructor (userGateway: UserGateway, infectionGateway: InfectionGateway) {
    this.userGateway = userGateway
    this.infectionGateway = infectionGateway
  }

  public async execute (userId: string, output: GetUserDetailsOutput): Promise<void> {
    const waitForUserGateway: Promise<User> = this.userGateway.getByUserId(userId)
    const waitForInfectionGateway: Promise<number> = this.infectionGateway.getTotalForUser(userId)

    const all: [User, number] = await Promise.all([
      waitForUserGateway,
      waitForInfectionGateway
    ])

    const response: UserDetailsResponse = {
      fullName: all['0'].fullName,
      potentialInfections: all['1']
    } as UserDetailsResponse

    output.presentUserDetails(response)
  }
}

export default GetUserDetails