interface GetUserDetailsInput {
  execute (userId: string, output: GetUserDetailsOutput): void;
}

interface GetUserDetailsOutput {
  presentUserDetails (response: UserDetailsResponse): void
}

interface UserDetailsResponse {
  fullName: string;
  potentialInfections: number;
}

export { GetUserDetailsInput, GetUserDetailsOutput, UserDetailsResponse }
