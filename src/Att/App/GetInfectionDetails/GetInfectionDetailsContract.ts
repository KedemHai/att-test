import { Infection } from '../../Domain/Infection'

interface GetInfectionDetailsInput {
  execute (infectionId: string, output: GetInfectionDetailsOutput): void
}

interface GetInfectionDetailsOutput {
  presentInfectionDetails (response: Infection): void
}

export { GetInfectionDetailsInput, GetInfectionDetailsOutput }