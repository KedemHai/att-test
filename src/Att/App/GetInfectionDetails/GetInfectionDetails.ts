import { GetInfectionDetailsInput, GetInfectionDetailsOutput } from './GetInfectionDetailsContract'
import { InfectionGateway } from '../InfectionGateway/InfectionGateway'
import { Infection } from '../../Domain/Infection'

class GetInfectionDetails implements GetInfectionDetailsInput {
  private readonly infectionGateway: InfectionGateway

  public constructor (infectionGateway: InfectionGateway) {
    this.infectionGateway = infectionGateway
  }

  public async execute (infectionId: string, output: GetInfectionDetailsOutput): Promise<void> {
    const infection: Infection = await this.infectionGateway.getByInfectionId(infectionId)
    output.presentInfectionDetails(infection)
  }
}

export default GetInfectionDetails