import { Infection } from '../../Domain/Infection'

interface InfectionGateway {
  getTotalForUser (userId: string): Promise<number>;

  getAllForUser (userId: string): Promise<Infection[]>

  getByInfectionId (infectionId: string): Promise<Infection>
}

export { InfectionGateway }