import { User } from '../../Domain/User'

interface UserGateway {
  getByUserId (userId: string): Promise<User>
}

export { UserGateway }