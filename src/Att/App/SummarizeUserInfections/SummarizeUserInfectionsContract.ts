interface SummarizeUserInfectionsInput {
  execute (userId: string, output: SummarizeUserInfectionsOutput): void
}

interface SummarizeUserInfectionsOutput {
  presentUserInfections (list: UserInfectionSummaryResponse[]): void
}

interface UserInfectionSummaryResponse {
  id: string;
  datetime: Date;
}

export { SummarizeUserInfectionsInput, SummarizeUserInfectionsOutput, UserInfectionSummaryResponse }