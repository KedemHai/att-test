import {
  SummarizeUserInfectionsInput,
  SummarizeUserInfectionsOutput,
  UserInfectionSummaryResponse
} from './SummarizeUserInfectionsContract'
import { InfectionGateway } from '../InfectionGateway/InfectionGateway'
import { Infection } from '../../Domain/Infection'

function sortByDateDesc (a: Infection, b: Infection): number {
  if (a.datetime.getTime() > b.datetime.getTime()) {
    return -1
  }

  if (a.datetime.getTime() < b.datetime.getTime()) {
    return 1
  }

  return 0
}

class SummarizeUserInfections implements SummarizeUserInfectionsInput {
  private readonly infectionGateway: InfectionGateway

  public constructor (infectionGateway: InfectionGateway) {
    this.infectionGateway = infectionGateway
  }

  public async execute (userId: string, output: SummarizeUserInfectionsOutput): Promise<void> {
    const list: Infection[] = await this.infectionGateway.getAllForUser(userId)
    output.presentUserInfections(list.sort(sortByDateDesc).map(value => {
      return {
        id: value.id,
        datetime: value.datetime
      } as UserInfectionSummaryResponse
    }))
  }
}

export default SummarizeUserInfections