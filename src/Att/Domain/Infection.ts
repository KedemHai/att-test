interface Infection {
  id: string;
  datetime: Date;
  lat: number;
  lon: number;
}

export { Infection }