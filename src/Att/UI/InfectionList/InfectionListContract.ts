import { UserInfectionSummaryResponse } from '../../App/SummarizeUserInfections/SummarizeUserInfectionsContract'

interface InfectionListUserActions {
  loaded (): void
}

interface InfectionListView {
  attach (userActions: InfectionListUserActions): void

  setList (list: UserInfectionSummaryResponse[]): void
}

export { InfectionListUserActions, InfectionListView }