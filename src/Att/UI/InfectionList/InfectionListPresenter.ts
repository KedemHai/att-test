import { InfectionListUserActions, InfectionListView } from './InfectionListContract'
import {
  SummarizeUserInfectionsInput,
  SummarizeUserInfectionsOutput,
  UserInfectionSummaryResponse
} from '../../App/SummarizeUserInfections/SummarizeUserInfectionsContract'

class InfectionListPresenter implements InfectionListUserActions, SummarizeUserInfectionsOutput {
  private readonly view: InfectionListView
  private readonly userId: string
  private readonly summarizeUserInfections: SummarizeUserInfectionsInput

  public constructor (view: InfectionListView, userId: string, summarizeUserInfections: SummarizeUserInfectionsInput) {
    this.view = view
    this.userId = userId
    this.summarizeUserInfections = summarizeUserInfections
  }

  public loaded (): void {
    this.summarizeUserInfections.execute(this.userId, this)
  }

  public presentUserInfections (list: UserInfectionSummaryResponse[]): void {
    this.view.setList(list)
  }
}

export default InfectionListPresenter