import { TextUserActions, TextView } from '../Text/TextContract'

class DateTimePresenter implements TextUserActions {
  private readonly view: TextView
  private readonly datetime: Date

  public constructor (view: TextView, datetime: Date) {
    this.view = view
    this.datetime = datetime
  }

  public loaded (): void {
    this.view.setText(this.datetime.toLocaleString())
  }
}

export default DateTimePresenter