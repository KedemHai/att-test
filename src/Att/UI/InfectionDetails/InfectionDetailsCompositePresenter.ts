import { GetInfectionDetailsOutput } from '../../App/GetInfectionDetails/GetInfectionDetailsContract'
import { Infection } from '../../Domain/Infection'

class InfectionDetailsCompositePresenter implements GetInfectionDetailsOutput {
  public presentInfectionDetails (_response: Infection): void {
    return
  }
}

export default InfectionDetailsCompositePresenter