import { TextUserActions, TextView } from '../Text/TextContract'

class LocationPresenter implements TextUserActions {
  private readonly view: TextView
  private readonly location: number

  public constructor (view: TextView, location: number) {
    this.view = view
    this.location = location
  }

  public loaded (): void {
    this.view.setText(String(this.location))
  }
}

export default LocationPresenter