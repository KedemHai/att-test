interface TextUserActions {
  loaded (): void
}

interface TextView {
  attach (userActions: TextUserActions): void

  setText (text: string): void
}

export { TextUserActions, TextView }