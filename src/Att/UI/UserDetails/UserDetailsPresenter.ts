import { UserDetailsUserActions, UserDetailsView } from './UserDetailsContract'
import {
  GetUserDetailsInput,
  GetUserDetailsOutput,
  UserDetailsResponse
} from '../../App/GetUserDetails/GetUserDetailsContract'

class UserDetailsPresenter implements UserDetailsUserActions, GetUserDetailsOutput {
  private readonly view: UserDetailsView
  private readonly userId:string
  private readonly getUserDetails: GetUserDetailsInput

  public constructor (view: UserDetailsView, userId: string, getUserDetails: GetUserDetailsInput) {
    this.view = view
    this.userId = userId
    this.getUserDetails = getUserDetails
  }

  public loaded (): void {
    this.getUserDetails.execute(this.userId, this)
  }

  public presentUserDetails (response: UserDetailsResponse): void {
    this.view.setUserName(response.fullName)
    this.view.setNumberOfInfections(response.potentialInfections)
  }
}

export default UserDetailsPresenter