interface UserDetailsUserActions {
  loaded (): void;
}

interface UserDetailsView {
  attach (userActions: UserDetailsUserActions): void;

  setUserName (name: string): void;

  setNumberOfInfections (n: number): void;
}

export { UserDetailsUserActions, UserDetailsView }