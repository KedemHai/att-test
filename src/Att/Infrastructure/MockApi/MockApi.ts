import { MockApiInfection, MockApiUser } from './MockApiContract'

function generateRandomLatLon (): number {
  return Math.round((Math.random() * 360 - 180) * 10000000) / 10000000
}

function generateRandomDate (fromDate: Date, toDate: Date): Date {
  const min = fromDate.getTime()
  const max = toDate.getTime()
  const rndDate = Math.floor(Math.random() * (max - min + 1) + min)
  return new Date(rndDate)
}

/**
 * Although this is done in memory and not over Http - It still illustrates the separation of this external service from the App's gateways.
 * For Example - Note that Infection gateway supports getting a single Infection by Id while the BE Api does not!
 */
class MockApi {
  public async getUserById (userId: string): Promise<MockApiUser> {
    return {
      id: Number(userId),
      fname: 'Poor',
      lname: 'Man'
    }
  }

  public async getInfectionsForUser (_userId: string): Promise<MockApiInfection[]> {
    const listInfections: MockApiInfection[] = []

    const theNow = Date.now()
    const lastFourteenDays = theNow - 14 * 24 * 60 * 60 * 1000
    for (let i = 0; i < 10000; i++) {
      const singleInfection = {} as MockApiInfection

      singleInfection.location = {
        lat: generateRandomLatLon(),
        lon: generateRandomLatLon()
      }

      const randomDatetime = generateRandomDate(new Date(lastFourteenDays), new Date(theNow))
      singleInfection.datetime = randomDatetime.toISOString()

      listInfections.push(singleInfection)
    }

    return listInfections
  }
}

export default MockApi