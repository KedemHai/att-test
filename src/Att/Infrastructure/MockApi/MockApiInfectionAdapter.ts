import { Infection } from '../../Domain/Infection'
import { MockApiInfection } from './MockApiContract'

class MockApiInfectionAdapter implements Infection {
  public static from (externalId: string, response: MockApiInfection): MockApiInfectionAdapter {
    return new MockApiInfectionAdapter(externalId, response)
  }

  private readonly externalId: string
  private readonly response: MockApiInfection

  public get id (): string {
    return this.externalId
  }

  public get datetime (): Date {
    return new Date(this.response.datetime)
  }

  public get lat (): number {
    return this.response.location.lat
  }

  public get lon (): number {
    return this.response.location.lon
  }

  private constructor (externalId: string, response: MockApiInfection) {
    this.externalId = externalId
    this.response = response
  }
}

export default MockApiInfectionAdapter