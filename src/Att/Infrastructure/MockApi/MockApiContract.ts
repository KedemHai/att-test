interface MockApiUser {
  id: number;
  fname: string;
  lname: string;
}

interface MockApiLocation {
  lon: number;
  lat: number;
}

interface MockApiInfection {
  location: MockApiLocation;
  datetime: string;
}

export { MockApiUser, MockApiLocation, MockApiInfection }