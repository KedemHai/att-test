import { UserGateway } from '../../App/UserGateway/UserGateway'
import { User } from '../../Domain/User'
import MockApi from './MockApi'
import { MockApiUser } from './MockApiContract'
import MockApiUserAdapter from './MockApiUserAdapter'

class MockApiUserGateway implements UserGateway {
  private readonly mockApi: MockApi

  public constructor (mockApi: MockApi) {
    this.mockApi = mockApi
  }

  public async getByUserId (userId: string): Promise<User> {
    const response: MockApiUser = await this.mockApi.getUserById(userId)
    return MockApiUserAdapter.from(response)
  }
}

export default MockApiUserGateway