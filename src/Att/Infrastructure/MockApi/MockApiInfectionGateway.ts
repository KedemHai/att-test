import { InfectionGateway } from '../../App/InfectionGateway/InfectionGateway'
import { Infection } from '../../Domain/Infection'
import MockApi from './MockApi'
import { MockApiInfection } from './MockApiContract'
import { v4 as Uuid } from 'uuid'
import { Deferred } from 'ts-deferred'
import MockApiInfectionAdapter from './MockApiInfectionAdapter'

class MockApiInfectionGateway implements InfectionGateway {
  private readonly mockApi: MockApi
  private cacheByUserId: Map<string, Infection[]> = new Map<string, Infection[]>()
  private cacheById: Map<string, Infection> = new Map<string, Infection>()
  private deferredSavingCache: Deferred<void> | null = null

  public constructor (mockApi: MockApi) {
    this.mockApi = mockApi
  }

  public async getByInfectionId (infectionId: string): Promise<Infection> {
    const exist = this.cacheById.get(infectionId)
    if (typeof exist === 'undefined') {
      throw new Error('Could not find Infection Id')
    }

    return exist
  }

  public async getTotalForUser (userId: string): Promise<number> {
    const list = await this.saveCache(userId)
    return list.length
  }

  public async getAllForUser (userId: string): Promise<Infection[]> {
    const list = await this.saveCache(userId)
    return [...list]
  }

  private async saveCache (userId: string): Promise<Infection[]> {
    if (this.deferredSavingCache !== null) {
      await this.deferredSavingCache.promise
    }

    const exist = this.cacheByUserId.get(userId)
    if (typeof exist !== 'undefined') {
      return exist
    }

    this.deferredSavingCache = new Deferred<void>()

    const response: MockApiInfection[] = await this.mockApi.getInfectionsForUser(userId)
    const withIds = this.saveIds(response)
    this.cacheByUserId.set(userId, withIds)

    this.deferredSavingCache.resolve()
    this.deferredSavingCache = null

    return withIds
  }

  private saveIds (list: MockApiInfection[]): Infection[] {
    this.cacheById.clear()
    return list.map(value => {
      const externalId = Uuid()
      const infection: Infection = MockApiInfectionAdapter.from(externalId, value)
      this.cacheById.set(externalId, infection)

      return infection
    })
  }
}

export default MockApiInfectionGateway