import { User } from '../../Domain/User'
import { MockApiUser } from './MockApiContract'

class MockApiUserAdapter implements User {
  public static from (response: MockApiUser): MockApiUserAdapter {
    return new MockApiUserAdapter(response)
  }

  private readonly response: MockApiUser

  public get fullName (): string {
    return `${this.response.fname} ${this.response.lname}`
  }

  private constructor (response: MockApiUser) {
    this.response = response
  }
}

export default MockApiUserAdapter