import { InfectionGateway } from '../../App/InfectionGateway/InfectionGateway'
import { Infection } from '../../Domain/Infection'

abstract class InfectionGatewayDecorator implements InfectionGateway {
  private readonly decorated: InfectionGateway

  protected constructor (decorated: InfectionGateway) {
    this.decorated = decorated
  }

  public getByInfectionId (infectionId: string): Promise<Infection> {
    return this.decorated.getByInfectionId(infectionId)
  }

  public getTotalForUser (userId: string): Promise<number> {
    return this.decorated.getTotalForUser(userId)
  }

  public listAllDatesForUser (userId: string): Promise<Date[]> {
    return this.decorated.listAllDatesForUser(userId)
  }
}

export default InfectionGatewayDecorator